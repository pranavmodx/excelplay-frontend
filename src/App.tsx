import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import PrivateRoute from './components/common/PrivateRoute/PrivateRoute';
import Home from './pages/Home/Home';
import Test from './pages/Test/Test';
import Spinner from './components/common/Spinner/Spinner';
import './App.scss';

import Authorisation from './components/Authorisation/Authorisation';

const App = () => {
  return (
    <div className="main-background">
      <Authorisation />
      <Suspense fallback={<Spinner />}>
        <Router>
          <Route exact path='/' component={Home} />
          <PrivateRoute exact path='/test' component={Test} />
        </Router>
      </Suspense>
    </div>
  );
};

export default App;